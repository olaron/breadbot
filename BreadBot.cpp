#include "bot/Game.h"

#include <random>
#include <thread>

#ifdef DEBUG
#include <chrono>
#endif

namespace bot {

struct BreadBot {
    hlt::Game halite_api;
    bot::Game game;
    std::mt19937 rng;

    explicit BreadBot(unsigned long seed)
    : halite_api(), game(halite_api), rng(seed)
    {
        hlt::log::log("Map width : " + std::to_string(game.map.width) + ", height : " + std::to_string(game.map.height) + ".");

        // At this point "game" variable is populated with initial map data.
        // This is a good place to do computationally expensive start-up pre-processing.
        // As soon as you call "ready" function below, the 2 second per turn timer will start.
        halite_api.ready("BreadBot");

        hlt::log::log("Successfully created bot! My Player ID is " + std::to_string(game.my_id) + ". Bot rng seed is " + std::to_string(seed) + ".");
    }

    void deposit_halite(bot::Entity& ship)
    {
        ship.ship_state = DEPOSIT;
        auto halite_ship = halite_api.me->ships.at(ship.id);
        auto move = halite_api.game_map->naive_navigate(halite_ship, halite_api.me->shipyard->position);
        if(move == hlt::Direction::STILL)
        {
            game.stay(ship);
            ship.ship_state = NONE;
            hlt::log::log("Ship id " + std::to_string(ship.id) + " is depositing ressources and goes back to get more");
        }
        else
        {
            game.move(ship, Position{move});
        }
    }

    void ship_action(bot::Entity& ship)
    {
        int median_gain = (int) floor(game.map.median_halite * 0.25);

        int move_cost = game.move_cost(ship.position);

        if(move_cost > ship.halite)
        {
            // On a pas assez de ressources pour bouger
            ship.ship_state = NONE;
            game.stay(ship);
            return;
        }


        int stay_gain = game.stay_gain(ship.position, ship.halite);

        if(game.me().shipyard.position == ship.position)
        {
            stay_gain -= 1000;
        }

        if(stay_gain > median_gain)
        {
            // On gagne suffisament de ressource là où on est
            game.stay(ship);
            return;
        }

        if(ship.ship_state == DEPOSIT)
        {
            hlt::log::log("Ship id " + std::to_string(ship.id) + " is on its way to the shipyard");
            deposit_halite(ship);
            return;
        }

        if(hlt::constants::MAX_HALITE - ship.halite > median_gain)
        {
            hlt::log::log("Ship id " + std::to_string(ship.id) + " is looking for ressources");
            // On peut essayer de bouger pour avoir plus de resources
            bot::Position current_direction = bot::Position::NORTH();

            int best_gain = stay_gain;
            std::optional<bot::Position> best_direction;

            for (int i = 0; i < 4; ++i) {
                current_direction = current_direction.rotate_CW();
                bot::Position position {ship.position};
                position = position + current_direction;

                int gain = game.stay_gain(position, ship.halite);

                auto other_ship = game.get_entity(game.map.get(position).ship);

                if(other_ship && (*other_ship)->owner == game.my_id)
                {
                     gain = -2 * hlt::constants::SHIP_COST;
                }

                if(other_ship && (*other_ship)->owner != game.my_id)
                {
                    gain = (*other_ship)->halite - ship.halite;
                }

                if(gain > best_gain)
                {
                    best_direction = current_direction;
                    best_gain = gain;
                }
            }

            if(best_direction)
            {
                auto colliding_ship = game.get_entity(game.map.get(ship.position + best_direction.value()).ship);
                if(colliding_ship && (*colliding_ship)->owner == game.my_id)
                {
                    game.stay(ship);
                }
                else
                {
                    game.move(ship, best_direction.value());
                }
            }
            else
            {
                game.stay(ship);
            }
        }
        else
        {
            hlt::log::log("Ship id " + std::to_string(ship.id) + " goes back to the shipyard");
            // On peut rentrer à la base déposer le stuff
            deposit_halite(ship);
        }
    }

    bool frame() {

        game.new_frame();

        for (auto& ship : game.my_ships()) {
            ship_action(*ship);
        }

        if (halite_api.turn_number <= 200 &&
            halite_api.me->halite >= hlt::constants::SHIP_COST &&
            !halite_api.game_map->at(halite_api.me->shipyard)->is_occupied())
        {
            game.commands.push_back(halite_api.me->shipyard->spawn());
        }

        return game.end_turn(halite_api);
    }

    void run() {
        while(frame()){}
    }
};

}

int main(int argc, char* argv[]) {

#ifdef DEBUG
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
#endif

    if(!bot::Position::test())
    {
        exit(1);
    }

    unsigned int rng_seed;
    if (argc > 1) {
        rng_seed = static_cast<unsigned int>(std::stoul(argv[1]));
    } else {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }

    bot::BreadBot bot(rng_seed);

    bot.run();

    return 0;
}
