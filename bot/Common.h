#pragma once

namespace bot {

typedef int EntityId;
typedef int PlayerId;


enum EntityType {
    SHIP,
    SHIPYARD,
    DROPOFF,
};

enum ShipState {
    NONE,
    DEPOSIT,
};

int mod(int a, int b);

}