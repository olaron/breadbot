#include "Map.h"

#include <algorithm>

namespace bot {

Map::Map(const hlt::GameMap &game_map)
        : width(game_map.width), height(game_map.height), map(game_map.width * game_map.height), average_halite(0),
          median_halite(0) {
    average_halite = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            average_halite += get(x, y).halite;
        }
    }
    average_halite /= width * height;
}

bot::Cell &Map::get(int x, int y) {
    return map.at(mod(x, width) + width * mod(y, height));
}

bot::Cell &Map::get(const bot::Position &position) {
    return get(position.x, position.y);
}

void Map::update(const hlt::Game &halite_api) {
    {
        average_halite = 0;
        std::vector<int> halite_values(map.size());
        const std::unique_ptr<hlt::GameMap> &halite_api_map = halite_api.game_map;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                hlt::MapCell *halite_cell = halite_api_map->at(hlt::Position{x, y});
                bot::Cell &cell = get(x, y);
                cell = Cell{*halite_cell};
                {
                    int halite = cell.halite;
                    halite_values.at(x + width * y) = halite;
                    average_halite += halite;
                }
                {

                }
            }
        }
        average_halite /= width * height;
        std::sort(halite_values.begin(), halite_values.end());
        median_halite = halite_values.at(halite_values.size() / 2);
    }
    {

    }
}

}