#pragma once

#include "Common.h"
#include "Entity.h"

#include "hlt/game.hpp"
#include <vector>

namespace bot {

struct Player {
    PlayerId id;
    int halite;
    Entity shipyard;
    std::vector<EntityId> ships;
    std::vector<EntityId> dropoffs;

    explicit Player (const hlt::Player& player);
};


}