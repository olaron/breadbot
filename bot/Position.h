#pragma once

#include "hlt/game.hpp"

#include <optional>

namespace bot {

struct Position {
    int x = 0;
    int y = 0;

    Position();

    Position(int x, int y);

    explicit Position(hlt::Direction direction);

    explicit Position(hlt::Position position);

    static bot::Position NORTH();

    static bot::Position SOUTH();

    static bot::Position EAST();

    static bot::Position WEST();

    bot::Position rotate_CCW() const;

    bot::Position rotate_CW() const;

    bot::Position operator+(const bot::Position &pos) const;

    bool operator==(const bot::Position &pos) const;

    bool operator!=(const bot::Position &pos) const;

    explicit operator hlt::Position() const;

    std::optional <hlt::Direction> to_direction() const;

    static bool test();
};

}