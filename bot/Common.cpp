#include "Common.h"

namespace bot {


int mod(int a, int b)
{
    // modulus that always returns positive
    return (a % b + b) % b;
}

}