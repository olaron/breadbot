#pragma once

#include "Common.h"

#include "hlt/game.hpp"
#include <optional>

namespace bot {

struct Cell {
    int halite;
    std::optional<EntityId> ship;
    std::optional<EntityId> structure;

    Cell();

    explicit Cell(const hlt::MapCell& cell);
};


}