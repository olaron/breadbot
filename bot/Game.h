#pragma once

#include "Player.h"
#include "Entity.h"
#include "Map.h"
#include "Position.h"
#include "Common.h"

#include "hlt/game.hpp"

namespace bot {

struct Game {
    hlt::Game& halite_api;
    PlayerId my_id;
    int turn_number;
    int halite;
    bot::Map map;

    std::vector<Player> players;
    std::unordered_map<EntityId, bot::Entity> entities;

    std::vector<hlt::Command> commands;

    explicit Game(hlt::Game& game);

    void new_frame();

    std::vector<Entity*> my_ships();

    std::optional<Entity*> get_entity(std::optional<EntityId> id);

    int move_cost(const bot::Position& position);

    int stay_gain(const bot::Position& ship_position, int ship_halite);

    void stay(const bot::Entity& ship);

    void move(const bot::Entity& ship, bot::Position direction);

    bool end_turn(hlt::Game& halite_api);

    bot::Player& me();
};


}

