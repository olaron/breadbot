#include "Entity.h"

namespace bot {

Entity::Entity(const hlt::Ship& ship)
        :   id(ship.id),
            owner(ship.owner),
            type(SHIP),
            position(ship.position),
            halite(ship.halite)
{}

Entity::Entity(const hlt::Shipyard& shipyard)
        :   id(shipyard.id),
            owner(shipyard.owner),
            type(SHIPYARD),
            position(shipyard.position),
            halite(0)
{}

Entity::Entity(const hlt::Dropoff& dropoff)
        :   id(dropoff.id),
            owner(dropoff.owner),
            type(DROPOFF),
            position(dropoff.position),
            halite(0)
{}

void Entity::update(const hlt::Ship& ship) {
    position = Position{ship.position};
    halite = ship.halite;
}

}