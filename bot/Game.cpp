#include "Game.h"

#include "hlt/constants.hpp"
#include "hlt/log.hpp"

#include <optional>
#include <vector>
#include <algorithm>
#include <cmath>
#include <unordered_set>

namespace bot {

Game::Game(hlt::Game& game)
        :   halite_api(game),
            my_id(game.my_id),
            turn_number(game.turn_number),
            halite(game.me->halite),
            map(*game.game_map),
            players(),
            entities(),
            commands()
{
    for(const auto& player : halite_api.players)
    {
        players.emplace_back(*player);
    }
}

void Game::new_frame()
{

    halite_api.update_frame();
    commands.clear();
    turn_number = halite_api.turn_number;
    map.update(halite_api);
    {
        std::unordered_set<EntityId> dead_entities;
        for(auto entity: entities)
        {
            dead_entities.insert(entity.first);
        }
        for(const auto& player : halite_api.players)
        {
            for(const auto& ship : player->ships)
            {
                dead_entities.erase(ship.first);
                if(entities.find(ship.first) == entities.end())
                {
                    entities.insert({ship.first,Entity{*ship.second}});
                }
                else
                {
                    entities.at(ship.first).update(*ship.second);
                }
            }
            for(const auto& dropoff : player->dropoffs)
            {
                dead_entities.erase(dropoff.first);
                if(entities.find(dropoff.first) == entities.end())
                {
                    entities.insert({dropoff.first,Entity{*dropoff.second}});
                }
            }
        }
        for(auto entity : dead_entities)
        {
            hlt::log::log("Entity id " + std::to_string(entity) + " has died");
            entities.erase(entity);
        }
    }

    players.clear();
    for(const auto& halite_player : halite_api.players)
    {
        players.emplace_back(*halite_player);
    }
}

std::vector<Entity*> Game::my_ships()
{
    std::vector<Entity*> ships;
    for (auto& entity_entry : entities)
    {
        Entity& entity = entity_entry.second;
        if(entity.owner == my_id && entity.type == SHIP)
        {
            ships.push_back(&entity);
        }
    }
    return ships;
}

std::optional<Entity*> Game::get_entity(std::optional<EntityId> id)
{
    if(!id)
    {
        return std::nullopt;
    }
    return &entities.at(id.value());
}

int Game::move_cost(const bot::Position& position)
{
    auto cell = map.get(position);
    return (int)ceil(cell.halite * 0.1);
}

int Game::stay_gain(const bot::Position& ship_position, int ship_halite)
{
    auto cell = map.get(ship_position);
    return std::min((int) ceil(cell.halite * 0.25), hlt::constants::MAX_HALITE - ship_halite);
}

void Game::stay(const bot::Entity& ship)
{
    auto halite_ship = halite_api.players.at(ship.owner)->ships.at(ship.id);
    commands.push_back(halite_ship->stay_still());

    halite_api.game_map->at((hlt::Position)ship.position)->mark_unsafe(halite_ship);
}

void Game::move(const bot::Entity& ship, bot::Position direction)
{
    auto halite_ship = halite_api.me->ships.at(ship.id);
    commands.push_back(halite_ship->move(direction.to_direction().value()));

    bot::Position position = ship.position;
    map.get(position).ship = std::nullopt;
    position = position + direction;
    auto other_ship = map.get(position).ship;
    if(other_ship && entities.at(other_ship.value()).owner == my_id)
    {
        hlt::log::log("Error: Collision with another of our own ship!");
    }
    map.get(position).ship = ship.id;
    halite_api.game_map->at((hlt::Position)position)->mark_unsafe(halite_ship);
}

bool Game::end_turn(hlt::Game& halite_api)
{
    {
        hlt::log::log("---");
        hlt::log::log("End of turn " + std::to_string(turn_number) + ":");
        for (const std::string &command: commands) {
            hlt::log::log(command);
        }
        hlt::log::log("---");
    }
    return halite_api.end_turn(commands);
}

bot::Player &Game::me() {
    return players.at(my_id);
}

};