#include "Position.h"

namespace bot {

Position::Position() = default;

Position::Position(int x, int y) : x(x), y(y) {}

Position::Position(hlt::Direction direction) {
    switch (direction) {
        case hlt::Direction::NORTH:
            *this = NORTH();
            return;
        case hlt::Direction::SOUTH:
            *this = SOUTH();
            return;
        case hlt::Direction::EAST:
            *this = EAST();
            return;
        case hlt::Direction::WEST:
            *this = WEST();
            return;
        case hlt::Direction::STILL:
            *this = {0, 0};
            return;
        default:
            hlt::log::log("Error: invalid direction in Position constructor");
            exit(1);
    }
}

Position::Position(hlt::Position position)
        : x(position.x), y(position.y) {}

bot::Position Position::NORTH() {
    return {0, -1};
}

bot::Position Position::SOUTH() {
    return {0, 1};
}

bot::Position Position::EAST() {
    return {1, 0};
}

bot::Position Position::WEST() {
    return {-1, 0};
}

bot::Position Position::rotate_CCW() const {
    return {y, -x};
}

bot::Position Position::rotate_CW() const {
    return {-y, x};
}

bot::Position Position::operator+(const bot::Position &pos) const {
    return {x + pos.x, y + pos.y};
}

bool Position::operator==(const bot::Position &pos) const {
    return x == pos.x && y == pos.y;
}

bool Position::operator!=(const bot::Position &pos) const {
    return !(pos == *this);
}

Position::operator hlt::Position() const {
    return {x, y};
}

std::optional<hlt::Direction> Position::to_direction() const {
    if (*this == NORTH()) {
        return hlt::Direction::NORTH;
    }
    if (*this == SOUTH()) {
        return hlt::Direction::SOUTH;
    }
    if (*this == EAST()) {
        return hlt::Direction::EAST;
    }
    if (*this == WEST()) {
        return hlt::Direction::WEST;
    }
    hlt::log::log("Error: invalid vector in Position::to_direction()");
    return std::nullopt;
}

bool Position::test() {
    bot::Position pos = NORTH();
    pos = pos.rotate_CW();
    if (pos != EAST()) return false;
    pos = pos.rotate_CW();
    if (pos != SOUTH()) return false;
    pos = pos.rotate_CW();
    if (pos != WEST()) return false;
    pos = pos.rotate_CW();
    if (pos != NORTH()) return false;
    pos = pos.rotate_CCW();
    if (pos != WEST()) return false;
    pos = pos.rotate_CCW();
    if (pos != SOUTH()) return false;
    pos = pos.rotate_CCW();
    if (pos != EAST()) return false;
    pos = pos.rotate_CCW();
    if (pos != NORTH()) return false;
    return true;
}

}