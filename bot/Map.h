#pragma once

#include "Cell.h"
#include "Position.h"

#include "hlt/game.hpp"
#include <vector>

namespace bot {

struct Map {
    int width;
    int height;
    std::vector<bot::Cell> map;

    int average_halite;
    int median_halite;

    explicit Map(const hlt::GameMap &game_map);

    bot::Cell &get(int x, int y);

    bot::Cell &get(const bot::Position &position);

    void update(const hlt::Game &halite_api);
};

}