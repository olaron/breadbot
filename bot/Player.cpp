#include "Player.h"

namespace bot {

Player::Player (const hlt::Player& player) :
        id(player.id),
        shipyard(*player.shipyard),
        halite(player.halite),
        ships(player.ships.size()),
        dropoffs(player.dropoffs.size())
{
    for(const auto& ship : player.ships)
    {
        ships.push_back(ship.first);
    }

    for(const auto& dropoff : player.dropoffs)
    {
        dropoffs.push_back(dropoff.first);
    }
}


}