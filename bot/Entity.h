#pragma once

#include "Common.h"
#include "Position.h"

namespace bot {

struct Entity {
    int id;
    PlayerId owner;
    EntityType type;
    bot::Position position;
    int halite;
    ShipState ship_state = NONE;

    explicit Entity(const hlt::Ship& ship);

    explicit Entity(const hlt::Shipyard& shipyard);

    explicit Entity(const hlt::Dropoff& dropoff);

    void update(const hlt::Ship& ship);
};


}