#include "Cell.h"

namespace bot {

Cell::Cell(): halite(0), ship(), structure(){}

Cell::Cell(const hlt::MapCell& cell)
{
    halite = cell.halite;

    if(cell.is_occupied()){
        ship = cell.ship->id;
    }

    if(cell.has_structure()){
        structure = cell.structure->id;
    }
}

}